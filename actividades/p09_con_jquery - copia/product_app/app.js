$(document).ready(function () {
    let edit = false; //Variable controlador de edición
    
    $('#product-result').hide(); //Se oculta la barra de estado
    listarProductos(); //Se llama a la función para listar los productos

    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function(response) {
                const productos = JSON.parse(response);
            
                //Se verifica que el objeto tenga datos
                if(Object.keys(productos).length > 0) {
                    //Se crea la plantilla para crear las filas de los elementos a insertar
                    let template = '';
                    productos.forEach(producto => {
                        let descripcion = '';
                        descripcion += '<li>precio: '+producto.precio+'</li>';
                        descripcion += '<li>unidades: '+producto.unidades+'</li>';
                        descripcion += '<li>modelo: '+producto.modelo+'</li>';
                        descripcion += '<li>marca: '+producto.marca+'</li>';
                        descripcion += '<li>detalles: '+producto.detalles+'</li>';
                    
                        template += `
                            <tr productId="${producto.id}">
                                <td>${producto.id}</td>
                                <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                <td><ul>${descripcion}</ul></td>
                                <td>
                                    <button class="product-delete btn btn-danger">
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        `;
                    });
                    // Se inserta la plantilla en el elemento "products"
                    $('#products').html(template);
                }
            }
        });
    }


    //Búsqueda de elementos por coincidencias en ID, NOMBRE O DESATALLES
    $('#search').keyup(function() {
        if($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: './backend/product-search.php?search='+$('#search').val(),
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        const productos = JSON.parse(response);
                        
                        //Se verifica que el objeto no este vacio
                        if(Object.keys(productos).length > 0) {
                            // Se crea la plantilla para insertar los elementos
                            let template = '';
                            let template_bar = ''; //Plantilla de la barra de estado

                            productos.forEach(producto => {
                                let descripcion = '';
                                descripcion += '<li>precio: '+producto.precio+'</li>';
                                descripcion += '<li>unidades: '+producto.unidades+'</li>';
                                descripcion += '<li>modelo: '+producto.modelo+'</li>';
                                descripcion += '<li>marca: '+producto.marca+'</li>';
                                descripcion += '<li>detalles: '+producto.detalles+'</li>';
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                        <td><ul>${descripcion}</ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;

                                template_bar += `
                                    <li>${producto.nombre}</il>
                                `;
                            });
                            // Se hace visible la barra de estado
                            $('#product-result').show();
                            // Se inserta la plantilla en el elemento "container"
                            $('#container').html(template_bar);
                            // Se inserta la plantilla en el elemento "products"
                            $('#products').html(template);    
                        }
                    }
                }
            });
        }
        else {
            $('#product-result').hide(); //Se oculta la barra de estado
        }
    });

    //Buscar coincidencias en el nombre
    $('#nombre').keyup(function() {
        if($('#nombre').val()) {
            let search = $('#nombre').val();
            $.ajax({
                url: './backend/product-nombre.php?search='+$('#nombre').val(),
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        const productos = JSON.parse(response);
                        
                        //Se verifica que el objeto no este vacio
                        if(Object.keys(productos).length > 0) {
                            // Se crea la plantilla para insertar los elementos
                            let template = '';
                            let template_bar = ''; //Plantilla de la barra de estado

                            productos.forEach(producto => {
                                let descripcion = '';
                                descripcion += '<li>precio: '+producto.precio+'</li>';
                                descripcion += '<li>unidades: '+producto.unidades+'</li>';
                                descripcion += '<li>modelo: '+producto.modelo+'</li>';
                                descripcion += '<li>marca: '+producto.marca+'</li>';
                                descripcion += '<li>detalles: '+producto.detalles+'</li>';
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                        <td><ul>${descripcion}</ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;

                                template_bar += `
                                    <span>Se encontraron coincidencias</span>
                                    <li>${producto.nombre}</il>
                                `;
                            });
                            // Se hace visible la barra de estado
                            $('#product-result').show();
                            // Se inserta la plantilla en el elemento "container"
                            $('#container').html(template_bar);
                            // Se inserta la plantilla en el elemento "products"
                            $('#products').html(template);    
                        }
                    }
                }
            });
        }
        else {
            $('#product-result').show(); //Se muestra la barra de estado
            $('#container').html("No se encontraron coincidencias"); //Se manda a imprimir que no se encontraron coincidencias
        }
    });

    //Existen muchas formas de elegir un nodo DOM, en este caso vamos a obtener
    //el formulario y cada campo de entrada del mismo, así como los elementos span
    //donde colocaremos los mensajes de error personalizados
    const nombre = document.getElementById('nombre');
    const e_nom = document.querySelector('#nombre + span.error'); //Para escribir los mensajes de error en esa área
    const marca = document.getElementById('marca');
    const e_mar = document.querySelector('#marca + span.error');
    const modelo = document.getElementById('modelo');
    const e_mod = document.querySelector('#modelo + span.error');
    const precio = document.getElementById('precio');
    const e_pre = document.querySelector('#precio + span.error');
    const detalles = document.getElementById('detalles');
    const e_det = document.querySelector('#detalles + span.error');
    const unidades = document.getElementById('unidades');
    const e_uni = document.querySelector('#unidades + span.error');


          //Este evento se realiza para cada dato a validar
          nombre.addEventListener('input', function (event){
              //Cada vez que el usuario escriba algo, se verificara si es válido
              if(nombre.validity.valid){
                 //Si el campo es válido se elimina el mensaje de error
                 e_nom.innerHTML = ''; //Restablece el contenido del mensaje
                 e_nom.className = 'error'; //Restablece el estado visual del mensaje
              }else{
                //Si el error persiste, se muestra especificamente cual es
                showError_nom();
              }
          });

          marca.addEventListener('select', function (event){
              //Cada vez que el usuario seleccione algo, se verificara si es válido
              if(marca.validity.valid){
                 //Si el campo es válido se elimina el mensaje de error
                 e_mar.innerHTML = ''; //Restablece el contenido del mensaje
                 e_mar.className = 'error'; //Restablece el estado visual del mensaje
              }else{
                //Si el error persiste, se muestra especificamente cual es
                showError_mar();
              }
          });

          modelo.addEventListener('input', function (event){
            //Cada vez que el usuario escriba algo, se verificara si es válido
            if(modelo.validity.valid){
                 //Si el campo es válido se elimina el mensaje de error
                 e_mar.innerHTML = ''; //Restablece el contenido del mensaje
                 e_mar.className = 'error'; //Restablece el estado visual del mensaje
              }else{
                //Si el error persiste, se muestra especificamente cual es
                showError_mod();
              }
          });

          precio.addEventListener('input', function (event){
            //Cada vez que el usuario escriba algo, se verificara si es válido
            if(precio.validity.valid){
                 //Si el campo es válido se elimina el mensaje de error
                 e_pre.innerHTML = ''; //Restablece el contenido del mensaje
                 e_pre.className = 'error'; //Restablece el estado visual del mensaje
              }else{
                //Si el error persiste, se muestra especificamente cual es
                showError_pre();
              }
          });

          detalles.addEventListener('textarea', function (event){
            //En caso de que el usuario escriba algo, se verificara si es válido
            if(detalles.validity.valid){
                 //Si el campo es válido se elimina el mensaje de error
                 e_pre.innerHTML = ''; //Restablece el contenido del mensaje
                 e_pre.className = 'e_det'; //Restablece el estado visual del mensaje
              }else{
                //Si el error persiste, se muestra especificamente cual es
                showError_det();
              }
          });

          unidades.addEventListener('input', function (event){
            //Cada vez que el usuario escriba algo, se verificara si es válido
            if(unidades.validity.valid){
                 //Si el campo es válido se elimina el mensaje de error
                 e_uni.innerHTML = ''; //Restablece el contenido del mensaje
                 e_uni.className = 'e_uni'; //Restablece el estado visual del mensaje
              }else{
                //Si el error persiste, se muestra especificamente cual es
                showError_uni();
              }
          });

          //Estas son las funciones que evaluan si los elementos son validos o no
          function showError_nom(){
              if(nombre.validity.valueMissing){
                  //Si el campo esta vacio se muestra el siguiente mensaje
                  e_nom.textContent = 'Este campo es obligatorio, introduzca un nombre.';
                  //Hacemos visible la barra de estado
                  $('#product-result').show();
                  //Colocamos el mensaje en la barra de estado
                  $('#container').html(e_nom.textContent);
              }else $('#product-result').hide(); //Se oculta la barra si ese no es el problema
              /*if(nombre.validity.tooShort){
                  //Si el campo es más corto que el mímino de caracteres
                  e_nom.textContent = `Ingresa al menos 5 caracteres. El número máximo de caracteres es ${ nombre.maxLength }; has introducido ${ nombre.value.length }`;
                  $('#product-result').show();
                  //Colocamos el mensaje en la barra de estado
                  $('#container').html(e_nom.textContent);
              } else $('#product-result').hide(); //Se oculta la barra si ese no es el problema*/
            //Se establece el estilo del error
            e_nom.className = 'error activo';
          }

          function showError_mar(){
              //Para este caso solo verificaremos que el campo no este vacío
              if(marca.validity.valueMissing){
                  e_mar.textContent = 'Este es un campo obligatorio, selecciona una opción';
                  //Hacemos visible la barra de estado
                  $('#product-result').show();
                  //Colocamos el mensaje en la barra de estado
                  $('#container').html(e_mar.textContent);
              } else $('#product-result').hide();
              //Se establece el estilo del error
              e_mar.className = 'error activo';
          }

          function showError_mod(){
              if(modelo.validity.valueMissing){
                  //Si el campo esta vacio se muestra el siguiente mensaje
                  e_mod.textContent = 'Este campo es obligatorio, introduzca el modelo.';
                  //Hacemos visible la barra de estado
                  $('#product-result').show();
                  //Colocamos el mensaje en la barra de estado
                  $('#container').html(e_mod.textContent);
              } else $('#product-result').hide();
              if(modelo.validity.tooShort){
                  //Si el campo no contiene el minimo de caracteres
                  e_mod.textContent = `Ingresa al menos 5 caracteres; has introducido ${modelo.value.length}. El número máximo es de ${modelo.maxLength}`;
                    //Hacemos visible la barra de estado
                  $('#product-result').show();
                  //Colocamos el mensaje en la barra de estado
                  $('#container').html(e_mod.textContent);
                } else $('#product-result').hide();
              if(modelo.validity.patternMismatch){
                  //Si el campo no contiene caracteres alfanuméricos mostrará el mensaje
                  e_mod.textContent = 'Este campo debe contener caracteres alfanúmericos';
                  //Hacemos visible la barra de estado
                  $('#product-result').show();
                  //Colocamos el mensaje en la barra de estado
                  $('#container').html(e_mod.textContent);
              } else $('#product-result').hide();
            //Se establece el estilo del error
            e_mod.className = 'error activo';
          }

          function showError_pre(){
            if(precio.validity.valueMissing){
                  //Si el campo esta vacio se muestra el siguiente mensaje
                  e_pre.textContent = 'Este campo es obligatorio, introduzca el precio.';
                  //Hacemos visible la barra de estado
                  $('#product-result').show();
                  //Colocamos el mensaje en la barra de estado
                  $('#container').html(e_pre.textContent);
              } else $('#product-result').hide();
            if(precio.validity.rangeUnderflow){
                  //Si el campo es menor al precio mínimo del producto
                  e_pre.textContent = `El precio mínimo del producto es de ${precio.min}; introduce un precio mayor`;
                  //Hacemos visible la barra de estado
                  $('#product-result').show();
                  //Colocamos el mensaje en la barra de estado
                  $('#container').html(e_pre.textContent);
            } else $('#product-result').hide();
            //Se establece el estilo del error
            e_pre.className = 'error activo';
          }

          function showError_det(){
              //Como este campo es opcional no se verificara que no este vacio
              if(detalles.validity.tooLong){ //Esta validación puede decirse que nunca arroja true, es muy raro que el navegador permita
                                             //ingresar más caracteres de los que se colocaron en el atributo maxLength
                  //En caso de usarse, verificar que no excede de máximo de caracteres de lo contrario
                e_det.textContent = `Excediste el número máximo de caracteres ${detalles.maxLength}; has introducido ${detalles.value.length}`;
                //Hacemos visible la barra de estado
                $('#product-result').show();
                //Colocamos el mensaje en la barra de estado
                $('#container').html(e_det.textContent);
            } else $('#product-result').hide();
              //Se establece el estilo del error
              e_det.className = 'error active';
          }

          function showError_uni(){
            if(unidades.validity.valueMissing){
                  //Si el campo esta vacio se muestra el siguiente mensaje
                  e_uni.textContent = 'Este campo es obligatorio, introduzca las unidades.';
                  //Hacemos visible la barra de estado
                  $('#product-result').show();
                  //Colocamos el mensaje en la barra de estado
                  $('#container').html(e_uni.textContent);
            } else $('#product-result').hide();
            if(!unidades.validity.rangeUnderflow){
                  //Si el campo es menor al precio mínimo del producto
                  e_uni.textContent = `Las unidades mínimas del producto son de ${unidades.min}; introduce valores correctos`;
                //Hacemos visible la barra de estado
                $('#product-result').show();
                //Colocamos el mensaje en la barra de estado
                $('#container').html(e_uni.textContent);
            } else $('#product-result').hide();
            //Se establece el estilo del error
            e_uni.className = 'error activo';
          }


    //Agregación o edición de productos
    $('#product-form').submit(e => {
        e.preventDefault();

        //Se crea el objeto que almacena los datos
        let postData = {
            id: $('#productId').val(),
            nombre: $('#nombre').val(),
            marca: $('#marca').val(),
            modelo: $('#modelo').val(),
            precio: $('#precio').val(),
            detalles: $('#detalles').val(),
            unidades: $('#unidades').val(),
            imagen: $('#imagen').val()
        }
        
        /**
         * AQUÍ DEBES AGREGAR LAS VALIDACIONES DE LOS DATOS EN EL JSON
         * --> EN CASO DE NO HABER ERRORES, SE ENVIAR EL PRODUCTO A AGREGAR
         **/
        let correcto = 0;
        //Llamamos a la función que validará el formulario
        if(!nombre.validity.valid){ //Si el error esta en el nombre, mostrar el mensaje de error
            showError_nom(); 
            e.preventDefault(); 
        }else correcto++;
        if(!marca.validity.valid){ //Si el error esta en la marca, mostrar el mensaje de error
            showError_mar();
            e.preventDefault();
        }else correcto++;
        if(!modelo.validity.valid){//Si el error esta en el modelo, mostrar el mensaje de error
            showError_mod();
            e.preventDefault();
        }else correcto++;
        if(!precio.validity.valid){ //Si el error esta en el precio, mostrar el mensaje de error
            showError_pre();
            e.preventDefault();
        }else correcto++;
        if(!detalles.validity.valid){ //Si el error esta en los detalles, mostrar el mensaje de error
            showError_det();
            e.preventDefault();
        }else if(!unidades.validity.valid){ //Si el error esta en las unidades, mostrar el mensaje de error
            showError_uni();
            e.preventDefault();
        }else {
            correcto++;
            $('#product-result').show();
            $('#container').innerHTML = "Campo válido"
        }

        let envio = true;
        //Validación de que ningún campo requerido este vacío
        if(correcto < 5){
            envio = false; //No se enviará el formulario
            //Se hace visible la barra de estado
            $('#product-result').show();
            //Se inserta la plantilla en el elemento container
            $('#container').html("Algunos o todos los campos requeridos estan vacíos");
        } else{
            envio = true;
            //Se hace visible la barra de estado
            $('#product-result').show();
            //Se inserta la plantilla en el elemento container
            $('#container').html("Se hará el envio a la Base de Datos");
        } 

        
        
        const url = edit === false ? './backend/product-add.php' : './backend/product-update.php';

        if(envio == true){
            $.post(url, postData, (response) => {
                console.log(response); //Respuesta del servidor en consola
                //Se obtiene el objeto de datos a través de un string
                let respuesta = JSON.parse(response);
                //Se crea una plantilla con la información para insertar en la barra de estado
                let template_bar = '';
                template_bar += `
                    <li style="list-style: none;">status: ${respuesta.status}</li>
                    <li style="list-style: none;">message: ${respuesta.message}</li>
                `;
                //Se reinician los campos del formulario
                $('#name').val('');
                $('#description').val(JsonString);
                //Se hace visible la barra de estado
                $('#product-result').show();
                //Se inserta la plantilla en el elemento container
                $('#container').html(template_bar);
                //Se listan los productos
                listarProductos();
                //Se regresa el valor de la variable controlador a su estado inicial
                edit = false;
            });
        }
    });


    //Eliminación de productos
    $(document).on('click', '.product-delete', (e) => {
        if(confirm('¿Realmente deseas eliminar el producto?')) {
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('productId');
            $.post('./backend/product-delete.php', {id}, (response) => {
                console.log(response); //Muestra la respuesta del servidor en consola
                let respuesta = JSON.parse(response);
                //Se crea una plantilla para la información de la barra de estado
                let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                //Se hace visible la barra de estado
                $('#product-result').show();
                //Se inserta la plantilla en el elemento "container"
                $('#container').html(template_bar);
                //Se listan los productos nuevamente
                listarProductos();
            });
        }
    });


    //Edición de productos
    $(document).on('click', '.product-item', (e) => {
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const id = $(element).attr('productId');
        $.post('./backend/product-single.php', {id}, (response) => {
            let product = JSON.parse(response);
            //Sse insertan los datos en los campos del formulario para su edición
            $('#nombre').val(product.nombre);
            $('#marca').val(product.marca);
            $('#modelo').val(product.modelo);
            $('#precio').val(product.precio);
            $('#detalles').val(product.detalles);
            $('#unidades').val(product.unidades);
            $('#imagen').val(product.imagen);
            //El ID se inserta en un campo oculto para el usuario, pero que será utilizado para la actualización
            $('#productId').val(product.id);
            //Variable controlador cambia a true
            edit = true;
        });
        e.preventDefault();
    });
});




