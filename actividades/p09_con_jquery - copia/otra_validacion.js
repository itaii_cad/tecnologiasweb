//Funciones de validación del formulario
        //Creamos una variable, que será la que almacene los errores para mostrar
        // en la barra de estado
        let errores = {}
        //Funciones de validación para los campos requeridos
        function valida_nombre(){
            console.log($('#nombre').val());
            //Se comprueba que no este vacío y sea menor a 100 caracteres
            if($('#nombre').val().length > 0 && $('#nombre').val().length < 100 ) {
                console.log('Nombre válido');
                return true;
            }else if( $('#nombre').val().length > 100){
                errores['Nombre_Length'] = 'El nombre excede el total de caracteres establecido';
                return false;
            }else{
                errores['Nombre'] = 'Este es un campo requerido';
                return false;
            }
        }

        function valida_marca(){
            console.log($('#marca').val());
            //Comprobamos que haya elegido una marca de la lista
            return $('#marca').val() ? true : false;
        }

        var pattern = "^[a-zA-Z0-9._%+-]$"
        function valida_modelo(pattern){
            console.log($('#modelo').val());
            if($('#modelo').val().length > 0 ){ //Campo requerido
                if($('#modelo').val().length < 25) { //De extensión menor a 25 caracteres
                    if($('#modelo').val().match(pattern) == true){ //Que tenga caracteres alfanúmerico
                        console.log("Modelo válido");
                        return true;
                    }else { 
                        errores['Modelo_Alfa'] = 'Este campo debe contener caracteres alfanúmericos';
                        return false;
                    }
                } else {
                    errores['Modelo_Length'] = 'El modelo excede el número de caracteres permitidos';
                    return false;
                }
            } else {
                errores['Modelo'] = 'Este es un campo requerido';
                return false;
            }
        }

        function valida_precio(){
            console.log($('#precio').val());
            if($('#precio').val().length > 0){ //requerido
                let precio = parseFloat($('#precio').val());
                if(precio >= 99.99){ //mayor o igual a 99.99
                    console.log("Precio válido");
                    return true;
                } else {
                    errores['Precio_Min'] = 'El precio mínimo del producto es de 99.99, digite nuevamente';
                    return false;
                }
            } else {
                errores['Precio'] = 'Este es un campo requerido';
                return false;
            }
        }

        function valida_detalles(){
            console.log($('#detalles').val());
            if($('#detalles').val() != ''){ //Si no esta vacío
                if($('#detalles').val().length < 250){ //Que sea menor a 250
                    console.log('Detalles válidos');
                    return true;
                } else {
                    errores['Detalles_Length'] = "El campo excede el número de caracteres permitido (250)";
                    return false;
                }
            } else {
                return true; //Valido aún si no se llena
            }
        }

        function valida_imagen(){
            console.log($('#imagen').val());
            if($('#imagen').val() != ''){ //Si esta vacio
                $('#imagen').val("img/imagen.png"); //Re asigna la ruta por defecto
                return true;
            } else {
                return true; //Si esta lleno pasa como válido al ser opcional
            }
        }

        function valida_unidades(){
            if($('#unidades').val().length > 0){ //Que no sea vacío
                let unidades = parseInt($('#unidades').val());
                if(unidades >= 0){ //Que su valor sean números reales
                    console.log("Unidades válidas");
                    return true;
                } else {
                    errores['Unidades_Val'] = 'Debe ingresar un número de unidades válidas mayor o igual a 0';
                    return false;
                }
            } else {
                errores['Unidades'] = 'Este es un campo requerido';
            }
        }

        function enableSubmit(){
            $('#product-form' + '#agregar').removeAttr("disabled");
        }

        function disableSubmit () {
            $('#product-form' + '#agregar').attr("disabled", "disabled");
        }

        function validar_form(){
            $('#product-form' + " *").on("change keydown", function() {
                //Verificamos que todo sea válido
                if (valida_nombre() && valida_marca() && valida_modelo(pattern) && 
                    valida_precio() && valida_detalles() && valida_unidades() &&
                    valida_imagen()) 
                { //Si todo es válido se activa el boton de enviar y se esconde la barra de estado
                    enableSubmit();
                    $('#product-result').hide();
                } else { //Si no es válido se muestran los errores en la barra de estado
                    //Se crea un plantilla para imprimir los errores
                    template_bar += `
                        <li style="list-style: none;"> ${errores.Nombre}</li>
                        <li style="list-style: none;"> ${errores.Modelo_Length}</li>
                        <li style="list-style: none;"> ${errores.Modelo}</li>
                        <li style="list-style: none;"> ${errores.Modelo_Length}</li>
                        <li style="list-style: none;"> ${errores.Modelo_Alfa}</li>
                        <li style="list-style: none;"> ${errores.Precio}</li>
                        <li style="list-style: none;"> ${errores.Precio_Min}</li>
                        <li style="list-style: none;"> ${errores.Detalles_Length}</li>
                        <li style="list-style: none;"> ${errores.Unidades}</li>
                        <li style="list-style: none;"> ${erroes.Unidades_Val}</li>
                    `;
                    // Se hace visible la barra de estado
                    $('#product-result').show();
                    // Se inserta la plantilla en el elemento "container"
                    $('#container').html(template_bar);
                    //Se desctiva el botón de enviar
                    disableSubmit();
                }
            });
        }