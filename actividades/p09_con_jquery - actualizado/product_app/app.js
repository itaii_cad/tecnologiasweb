$(document).ready(function () {
    let edit = false; //Variable controlador de edición
    
    $('#product-result').hide(); //Se oculta la barra de estado
    listarProductos(); //Se llama a la función para listar los productos

    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function(response) {
                const productos = JSON.parse(response);
            
                //Se verifica que el objeto tenga datos
                if(Object.keys(productos).length > 0) {
                    //Se crea la plantilla para crear las filas de los elementos a insertar
                    let template = '';
                    productos.forEach(producto => {
                        let descripcion = '';
                        descripcion += '<li>precio: '+producto.precio+'</li>';
                        descripcion += '<li>unidades: '+producto.unidades+'</li>';
                        descripcion += '<li>modelo: '+producto.modelo+'</li>';
                        descripcion += '<li>marca: '+producto.marca+'</li>';
                        descripcion += '<li>detalles: '+producto.detalles+'</li>';
                    
                        template += `
                            <tr productId="${producto.id}">
                                <td>${producto.id}</td>
                                <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                <td><ul>${descripcion}</ul></td>
                                <td>
                                    <button class="product-delete btn btn-danger">
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        `;
                    });
                    // Se inserta la plantilla en el elemento "products"
                    $('#products').html(template);
                }
            }
        });
    }


    //Búsqueda de elementos por coincidencias en ID, NOMBRE O DESATALLES
    $('#search').keyup(function() {
        if($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: './backend/product-search.php?search='+$('#search').val(),
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        const productos = JSON.parse(response);
                        
                        //Se verifica que el objeto no este vacio
                        if(Object.keys(productos).length > 0) {
                            // Se crea la plantilla para insertar los elementos
                            let template = '';
                            let template_bar = ''; //Plantilla de la barra de estado

                            productos.forEach(producto => {
                                let descripcion = '';
                                descripcion += '<li>precio: '+producto.precio+'</li>';
                                descripcion += '<li>unidades: '+producto.unidades+'</li>';
                                descripcion += '<li>modelo: '+producto.modelo+'</li>';
                                descripcion += '<li>marca: '+producto.marca+'</li>';
                                descripcion += '<li>detalles: '+producto.detalles+'</li>';
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                        <td><ul>${descripcion}</ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;

                                template_bar += `
                                    <li>${producto.nombre}</il>
                                `;
                            });
                            // Se hace visible la barra de estado
                            $('#product-result').show();
                            // Se inserta la plantilla en el elemento "container"
                            $('#container').html(template_bar);
                            // Se inserta la plantilla en el elemento "products"
                            $('#products').html(template);    
                        }
                    }
                }
            });
        }
        else {
            $('#product-result').hide(); //Se oculta la barra de estado
        }
    });


    //Función para validar los datos antes de que se envíen al servidor
    function valida_datos(data){
        //-------------------------------AQUÍ COMIENZAN LAS VALIDACIONES--------------------------------------
        var correcto = 0; //Variable controlador de la validación
        
        //---------------------------VALIDACIÓN DEL NOMBRE---------------------------------------------
        if ( data.nombre.length !== 0 ) { // Se valida que el nombre  no este vacío ya que es obligatorio
            if(data.nombre.length <= 100){  //Si no esta vacío se comprueba que no exceda el número de caracteres permitidos
                console.log("Ha ingresado un nombre correcto");
                correcto++;
            }else{
                console.log("Ha excedido el número de caracteres permitidos (100). Ha ingresado un total de (", data.nombre.length,")");
            }
        }else{
            console.log("Este es un campo obligatorio. Ingrese un nombre");
        }
      
      
        //---------------------------VALIDACIÓN DE LA MARCA--------------------------------------------
        //Variable con las marcas que puede ingresar el usuario
        let marcas = ['Editorial Castillo', 'Editorial Planeta', 'Editorial Porrúa', 'Editorial Alfaguara', 'Editorial Nube de Tinta'];
      
        let mar_val = false; //Controlador para validar si lo ingresado por el usuario se encuentra en la lista de marcas
        let c = 0; //Controlador de la lista de marcas
      
        if(data.marca.length !== 0){ //Primero se verifica que la variable no este vacía ya que es obligatoria
            while (c < marcas.length && !mar_val) { //Se hace un recorrido por la lista
                if (data.marca == marcas[c]) { //Ahora se verifica que lo que haya ingresado el usuario se encuentre en la lista
                    mar_val = true; //El controlador de validación cambiará a true
                }else mar_val = false; c++; // Sino, se seguira recorriendo la lista
            }
      
            if( mar_val ){ //Si mar_val es = true, significa que el usuario ha ingresado una marca de las que se encuentran en la lista
                console.log("Se seleccionó una marca de la lista: ", data.marca);
                correcto++;
            } else console.log("Ingresó una marca que no se encuentra entre las opciones");
        }else{
            console.log("Este es un campo obligatorio. Ingrese una Marca");
        }
      
      
        //-----------------------------VALIDACIÓN DEL MODELO---------------------------------------------
        let validacion = /[A-Za-z0-9.!#$%&'*+/=?^_`{|}~-]+/;
        if(data.modelo.length !== 0){ //Se comprueba que el campo no este vacío, porque es obligatorio
            if ( data.modelo.length <= 25 ) { //Sino esta vacío se comprueba que no exceda de los 25 caracteres
                if(validacion.test(data.modelo) !== false){ //Se comprueba que tenga caracteres alfanumericos
                    console.log("Ha ingresado un modelo correcto", data.modelo);
                    correcto++;
                }else{
                    console.log("El modelo debe contener caracteres alfanuméricos.");
                }
            }else{
                console.log("La longitud del modelo excede el rango establecido (25): ", data.modelo.length);
            }
        }else{
            console.log("Este es un campo obligatorio. Ingrese el modelo.");
        }
      
      
        //-----------------------------VALIDACIÓN DEL PRECIO----------------------------------------------
        data.precio = parseFloat(data.precio);
        if(data.precio.length !== 0){ //Comprobamos que el campo no este vacío, ya que es obligatorio
            if ( data.precio >= 99.99) { //Si no esta vacío se comprueba que sea mayor de 99.99
                console.log("Se ingresó un precio correcto");
                correcto++;
            }else{
                console.log("El precio es menor al rango establecido (99.99). Digitelo nuevamente");
            }
        }else{
            console.log("Este es un campo obligatorio. Ingrese un precio.")
        }
      
      
        //--------------------------VALIDACIÓN DE LOS DETALLES---------------------------------------------
        if(data.detalles !== ''){ //Si el campo detalles se lleno
            if ( data.detalles.length <= 250 ) { //Se comprueba que estos no excedan el límite de caracteres en el rango establecido
                console.log("Ingresó los detalles en el rango de caracteres permitido");
                correcto++;
            }else console.log("Ha excedido el límite de caracteres en este campo, reduzca su ingreso.");
        }else{
            console.log("No se añadiran detalles");
            correcto++; //Al no ser un campo obligatorio pasa como correcto aun si no se llena
        }
         
      
        //--------------------------VALIDACIÓN DE LAS UNIDADES--------------------------------------------
        data.unidades = parseFloat(data.unidades);
        if ( data.unidades > 0) {
            console.log("La Cantidad de unidades se encuentra en el rango establecido");
            correcto++;
        }else console.log("Este es un campo obligatorio por favor ingrese la unidades o en su defecto ingrese 0");
      
        //---------------------------VALIDACIÓN DE LA IMAGEN----------------------------------------------
        if(data.imagen == ''){ //Al no ser un campo obligatorio, se notifica al usuario que se establece una imagen por defecto
            data.imagen = 'img/imagen.png';
            console.log("No se ingresó la ruta, se asignara la ruta por defecto (img/imagen.png)");
            correcto++;
        }else{
            console.log("Usted ingresó la ruta: ", data.imagen);
            correcto++;
        }
      
        return correcto == 7 ? true : false; //Si al final correcto tiene como valor final 7
                                                  //significa que la variable envío será true
                                                  //y por tanto, el usuario ha ingresado los datos correctamente
                                                  //Por lo que enviará al servidor la solicitud de inserción, de lo contrario
                                                  //no se enviará al servidor ya que todo debe estar correcto
      
    }


    //Buscar coincidencias en el nombre
    $('#nombre').keyup(function() {
        if($('#nombre').val()) {
            let search = $('#nombre').val();
            $.ajax({
                url: './backend/product-nombre.php?search='+$('#nombre').val(),
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        console.log(response);

                        const productos = JSON.parse(response);
                        
                        //Se verifica que el objeto no este vacio
                        if(Object.keys(productos).length > 0) {
                            // Se crea la plantilla para insertar los elementos
                            let template = '';
                            let template_bar = ''; //Plantilla de la barra de estado

                            productos.forEach(producto => {
                                let descripcion = '';
                                descripcion += '<li>precio: '+producto.precio+'</li>';
                                descripcion += '<li>unidades: '+producto.unidades+'</li>';
                                descripcion += '<li>modelo: '+producto.modelo+'</li>';
                                descripcion += '<li>marca: '+producto.marca+'</li>';
                                descripcion += '<li>detalles: '+producto.detalles+'</li>';
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                        <td><ul>${descripcion}</ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;

                                template_bar += `
                                    <span>Se encontraron coincidencias</span>
                                    <li>${producto.nombre}</il>
                                `;
                            });
                            // Se hace visible la barra de estado
                            $('#product-result').show();
                            // Se inserta la plantilla en el elemento "container"
                            $('#container').html(template_bar);
                            // Se inserta la plantilla en el elemento "products"
                            $('#products').html(template);    
                        }
                    }
                }
            });
        }
        else {
            $('#product-result').show(); //Se muestra la barra de estado
            $('#container').html("No se encontraron coincidencias"); //Se manda a imprimir que no se encontraron coincidencias
        }
    });

    //Agregación o edición de productos
    $('#product-form').submit(e => {
        e.preventDefault();

        //Se crea el objeto que almacena los datos
        let postData = {
            id: $('#productId').val(),
            nombre: $('#nombre').val(),
            marca: $('#marca').val(),
            modelo: $('#modelo').val(),
            precio: $('#precio').val(),
            detalles: $('#detalles').val(),
            unidades: $('#unidades').val(),
            imagen: $('#imagen').val()
        }
        /**
         * AQUÍ DEBES AGREGAR LAS VALIDACIONES DE LOS DATOS EN EL JSON
         * --> EN CASO DE NO HABER ERRORES, SE ENVIAR EL PRODUCTO A AGREGAR
         **/
         let envio = false; //Variable controladora del envío al servidor
         if ( valida_datos(postData)) {
             envio = true; //Si son válidos únicamente se enviarán al servidor 
         } else{
             window.alert("Los datos son incorrectos"); //Si son incorrectos se enviará un mensaje al usuario
         }

        const url = edit === false ? './backend/product-add.php' : './backend/product-update.php';
        if(envio == true) {
            $.post(url, postData, (response) => {
                console.log(response); //Respuesta del servidor en consola
                //Se obtiene el objeto de datos a través de un string
                let respuesta = JSON.parse(response);
                //Se crea una plantilla con la información para insertar en la barra de estado
                let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                //Se reinician los campos del formulario
                $('#nombre').val('');
                $('#marca').val('');
                $('#modelo').val('');
                $('#precio').val('');
                $('#detalles').val('');
                $('#unidades').val('');
                $('#imagen').val('');
                //Se hace visible la barra de estado
                $('#product-result').show();
                //Se inserta la plantilla en el elemento container
                $('#container').html(template_bar);
                //Se listan los productos
                listarProductos();
                //Se regresa el valor de la variable controlador a su estado inicial
                edit = false;
            });
        }
    });


    //Eliminación de productos
    $(document).on('click', '.product-delete', (e) => {
        if(confirm('¿Realmente deseas eliminar el producto?')) {
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('productId');
            $.post('./backend/product-delete.php', {id}, (response) => {
                console.log(response); //Muestra la respuesta del servidor en consola
                let respuesta = JSON.parse(response);
                //Se crea una plantilla para la información de la barra de estado
                let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                //Se hace visible la barra de estado
                $('#product-result').show();
                //Se inserta la plantilla en el elemento "container"
                $('#container').html(template_bar);
                //Se listan los productos nuevamente
                listarProductos();
            });
        }
    });


    //Edición de productos
    $(document).on('click', '.product-item', (e) => {
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const id = $(element).attr('productId');
        $.post('./backend/product-single.php', {id}, (response) => {
            let product = JSON.parse(response);
            //Sse insertan los datos en los campos del formulario para su edición
            $('#nombre').val(product.nombre);
            $('#marca').val(product.marca);
            $('#modelo').val(product.modelo);
            $('#precio').val(product.precio);
            $('#detalles').val(product.detalles);
            $('#unidades').val(product.unidades);
            $('#imagen').val(product.imagen);
            //El ID se inserta en un campo oculto para el usuario, pero que será utilizado para la actualización
            $('#productId').val(product.id);
            //Variable controlador cambia a true
            edit = true;
        });
        e.preventDefault();
    });
});




