<?php
    include_once __DIR__.'/API/Productos.php';

    //SE CREA EL OBJETO DE LA CLASE
    $agregar = new Productos();

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    if(isset($_POST['nombre'])) {
        // SE TRANSFORMA EL POST A UN STRING EN JSON, Y LUEGO A OBJETO
        $jsonOBJ = json_decode( json_encode($_POST) );
        //SE LLAMA A LA FUNCIÓN ADD PASANDO EL OBJETO
        $agregar->add($jsonOBJ);
        
    }

    // SE MANDA EL RESULTADO A LA PÁGINA
    echo $agregar->getResponse();
?>