<?php
    include_once __DIR__.'/API/Productos.php';

    // SE CREA EL OBJETO DE LA CLASE
    $sigle_datos = new Productos();

    if( isset($_POST['id']) ) {
        $id = $_POST['id'];
        //SE LLAMA A LA FUNCION SINGLE PASANDO COMO PARAMETRO ID
        $sigle_datos->single($id);
    }

    // SE MANDA EL RESULTADO A LA PÁGINA
    echo $sigle_datos->getResponse();
?>