<?php
    include_once __DIR__.'/API/Productos.php';

    // SE CREA EL OBJETO DE LA CLASE
    $elimina = new Productos();
    // SE VERIFICA HABER RECIBIDO EL ID
    if( isset($_POST['id']) ) {
        $id = $_POST['id'];
        //SE LLAMA A LA FUNCIÓN DELETE PASANDO COMO PARAMETRO EL ID
        $elimina->delete($id);
    } 
    
    // SE MANDA EL RESULTADO A LA PÁGINA
    echo $elimina->getResponse();
?>