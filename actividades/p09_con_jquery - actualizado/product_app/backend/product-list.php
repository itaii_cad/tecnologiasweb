<?php
    include_once __DIR__.'/API/Productos.php';

    //Se crea un objeto de la clase
    $lista_productos = new Productos();
    //Se llama a la función de listar
    $lista_productos->list();
    //Se manda a la pagina el resultado
    echo $lista_productos->getResponse();
    
?>