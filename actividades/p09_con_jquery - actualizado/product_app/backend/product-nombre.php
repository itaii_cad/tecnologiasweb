<?php
    include_once __DIR__.'/API/Productos.php';

    // SE CREA EL OBJETO DE LA CLASE
    $busca_nombre = new Productos();
    // SE VERIFICA HABER RECIBIDO EL ID
    if( isset($_GET['search']) ) {
        $search = $_GET['search'];
        //SE LLAMA A LA FUNCION SINGLEBYNAME PASANDO SEARCH COMO PARAMETRO
        $busca_nombre->singleByName($search);
    } 
    
    // SE MANDA EL RESULTADO A APP
    echo $busca_nombre->getResponse();
?>