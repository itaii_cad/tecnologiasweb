<?php
    include_once __DIR__.'/API/Productos.php';

    // SE CREA EL OBJETO DE LA CLASE
    $editar = new Productos();
    // SE VERIFICA HABER RECIBIDO EL ID
    if( isset($_POST['id']) ) {
        $jsonOBJ = json_decode( json_encode($_POST) );
        //SE LLAMA A LA FUNCION EDIT PASANDO EL OBJETO COMO PARAMETRO
        $editar->edit($jsonOBJ);
    } 
    
    // SE MANDA EL RESULTADO A APP
    echo $editar->getResponse();
?>