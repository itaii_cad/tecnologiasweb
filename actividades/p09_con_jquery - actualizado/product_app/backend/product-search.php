<?php
    include_once __DIR__.'/API/Productos.php';

    // SE CREA EL OBJETO DE LA CLASE QUE SE VA A DEVOLVER
    $busqueda = new Productos();
    // SE VERIFICA HABER RECIBIDO EL ID
    if( isset($_GET['search']) ) {
        $search = $_GET['search'];
        //SE LLAMA A LA FUNCION SEARCH PASANDO COMO PARAMETRO EL ID
        $busqueda->search($search);
    } 
    
    //SE MANDA EL RESULTADO A LA PAGINA
    echo $busqueda->getResponse();
?>